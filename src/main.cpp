#include <SeqSLAM.h>
#include <NOSeqSLAM.h>
#include <MatchMatrix.h>



int main() {
    // build association matrices
    noseq::main();
    seq::main();

    // get precision and recall
    // mm::precisionAndRecallExperiment("../results/noseqslam/associations/", "../results/precision_and_recall/");
    // mm::precisionAndRecallExperiment("../results/seqslam/associations/", "../results/precision_and_recall/");

    // check running times
    // noseq::runningTimes();
    // seq::runningTimes();

    return 0;
}
