//
// Created by jurica on 08/04/19.
//

#ifndef VISUALPLACERECOGNITION_TYPES_H
#define VISUALPLACERECOGNITION_TYPES_H

#include <vector>
#include <functional>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>

namespace ty {
    using imageRepresentation = std::vector<float>;
    using associationFunction = std::function<float(const imageRepresentation&, const imageRepresentation&)>;
    using associationMatrixColumn = std::vector<float>;
    using matchPair = std::pair<int, int>;


    /*
    TP (true positive) - a pair that belongs to the ground truth
    TN (true negative) - does not exist as hypothesis that there exists an appropriate match for each I_q holds
    FN (false negative) - "unmacthed" pair due to the low similarity measure/ high difference measure
    FP - although similarity measure is high enough / difference measure is low enough, pair does not belong to the ground truth
    */
    enum class match { TN, FN, FP, TP };
    using matchMatrixColumn = std::vector<match>;


    const cv::Scalar colorTN{75, 75, 75};
    const cv::Scalar colorFN{185, 185, 185};
    const cv::Scalar colorFP{99, 30, 233};
    const cv::Scalar colorTP{255, 117, 12};

}


#endif //VISUALPLACERECOGNITION_TYPES_H
