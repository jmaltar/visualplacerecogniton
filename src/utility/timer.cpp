#include <timer.h>

Timer::Timer(): begin{clock()} {}
Timer::~Timer() = default;
double Timer::secondsByFar() const {
    clock_t end{clock()};
    return double(end - begin) / CLOCKS_PER_SEC;
}
