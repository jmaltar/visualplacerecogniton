//
// Created by jurica on 08/04/19.
//

#include <file.h>
#include <iostream>
#include <boost/algorithm/string.hpp>

namespace fl {
    std::vector<std::string> filePaths(const std::string& path) {
        // folder items iterator
        boost::filesystem::directory_iterator it = boost::filesystem::directory_iterator{path};

        // cast paths to vector in order to sort them
        std::vector<std::string> paths{};

        for(const auto& el: it)
            paths.push_back(el.path().string());

        // sort path by name
        std::sort(paths.begin(), paths.end());

        return paths;
    }

    void createDirectory(const std::string& path) {
        boost::filesystem::create_directory(path);
    }

    void savePrecisionAndRecall(const std::string& path, const std::string& fileName, const std::vector<std::pair<float, float>>& values) {
        std::ofstream file;
        file.open(path + fileName);
        unsigned int index{0u};
        for(const std::pair<float, float>& value: values)
            file << std::to_string(++index) << " " << std::to_string(value.first) << " " << std::to_string(value.second) << "\n";

        file.close();
    }

    std::vector<std::string> listDirectory(const std::string& path) {
        std::vector<std::string> paths;
        boost::filesystem::path p{path};
        std::vector<boost::filesystem::directory_entry> v; // To save the file names in a vector.

        if(boost::filesystem::is_directory(p))
        {
            copy(boost::filesystem::directory_iterator(p), boost::filesystem::directory_iterator(), back_inserter(v));

            for (std::vector<boost::filesystem::directory_entry>::const_iterator it = v.begin(); it != v.end();  ++ it)
                paths.push_back((*it).path().string());
        }
        std::sort(paths.begin(), paths.end());
        return paths;
    }

    std::vector<std::string> split(const std::string& text, char delimiter) {
        std::vector<std::string> results;
        boost::split(results, text, [&delimiter](char c){return c == delimiter;});
        return results;
    }

    std::string extension(const std::string& text) {
        return split(text, '.').back();
    }
}

