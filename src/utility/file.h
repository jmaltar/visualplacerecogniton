//
// Created by jurica on 08/04/19.
//

#ifndef VISUALPLACERECOGNITION_FILE_H
#define VISUALPLACERECOGNITION_FILE_H

#include <vector>
#include <string>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include <AssociationMatrix.h>

namespace fl {
    std::vector<std::string> filePaths(const std::string& path);

    void createDirectory(const std::string& path);

    template <typename T>
    void serializeToFile(const T& m, const std::string& path) {
        {
            std::ofstream ofs(path);
            boost::archive::text_oarchive oa(ofs);
            oa << m;
        }
    }

    template <typename T>
    void serializeFromFile(T& m, const std::string& path) {
        {
            std::ifstream ifs(path);
            boost::archive::text_iarchive ia(ifs);
            ia >> m;
        }
    }

    void savePrecisionAndRecall(const std::string& path, const std::string& fileName, const std::vector<std::pair<float, float>>& values);

    std::vector<std::string> listDirectory(const std::string& path);

    std::vector<std::string> split(const std::string&, char delimiter);

    std::string extension(const std::string&);

}



#endif //VISUALPLACERECOGNITION_FILE_H
