#ifndef VISUALPLACERECOGNITION_MATCHMATRIX_H
#define VISUALPLACERECOGNITION_MATCHMATRIX_H

#include <vector>
#include <AssociationMatrix.h>
#include <types.h>

namespace mm {
    struct MatchMatrix {
        std::vector<ty::matchMatrixColumn> container;

        MatchMatrix();

        // load ground truth from file
        MatchMatrix(const std::string& filePath, unsigned int queryLength, unsigned int referenceLength);

        // construct match matrix from association matrix and ground truth matrix
        MatchMatrix(const am::AssociationMatrix& m, const MatchMatrix& gt, float threshold=0.0f);

        ty::matchMatrixColumn& operator[](int i);
        const ty::matchMatrixColumn& operator[](int i) const;

        unsigned int queryLength() const;
        unsigned int referenceLength() const;

    };

    std::pair<float, float> precisionAndRecall(const MatchMatrix& n);
    std::vector<std::pair<float, float>> precisionAndRecallVector(const am::AssociationMatrix& m, const MatchMatrix& gt);
    void precisionAndRecallExperiment(const std::string& root, const std::string& destination);
}

#endif