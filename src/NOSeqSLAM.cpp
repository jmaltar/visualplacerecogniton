#include <NOSeqSLAM.h>
#include <img.h>
#include <iostream>
#include <timer.h>

namespace noseq {

    float shortestPathAssociation(const am::AssociationMatrix& m, am::AssociationMatrix& dist, am::AssociationMatrix& sim, unsigned int i, unsigned int j, unsigned d_s, int expRate) {

        float accLeft{0};
        float accRight{0};
        int i_offset, j_offset;
        int i_current_right, j_current_right, i_current_left, j_current_left;
        int i_parent_offset, j_parent_offset;
        int i_parent_left, j_parent_left, i_parent_right, j_parent_right;
        bool leftExp, rightExp;
        float betterApproximation, currentSimilarity;

        unsigned int Q{m.queryLength()};
        unsigned int R{m.referenceLength()};

        sim[i][j] = 0.0f;
        dist[i][j] = 0.0f;

        for(i_offset = 1; i_offset <= d_s; ++i_offset) {
            for(j_offset = 0; j_offset <= i_offset * (expRate - 1); ++j_offset) {
                i_current_right = i + i_offset;
                j_current_right = j + j_offset;
                i_current_left = i - i_offset;
                j_current_left = j - j_offset;

                leftExp = 0 <= i_current_left && i_current_left < Q && 0 <= j_current_left && j_current_left < R;
                rightExp = 0 <= i_current_right && i_current_right < Q && 0 <= j_current_right && j_current_right < R;


                i_parent_offset = i_offset - 1;
                for(j_parent_offset = j_offset; j_parent_offset >= j_offset - expRate + 1; --j_parent_offset) {
                    if (j_parent_offset >= 0 && j_parent_offset <= i_parent_offset * (expRate - 1)) {
                        i_parent_left = i - i_parent_offset;
                        j_parent_left = j - j_parent_offset;
                        i_parent_right = i + i_parent_offset;
                        j_parent_right = j + j_parent_offset;


                        if (leftExp) {
                            // relax
                            betterApproximation = dist[i_parent_left][j_parent_left] + 1.0f - m[i_current_left][j_current_left];
                            if (dist[i_current_left][j_current_left] > betterApproximation) {
                                dist[i_current_left][j_current_left] = betterApproximation;
                                sim[i_current_left][j_current_left] = sim[i_parent_left][j_parent_left] + m[i_current_left][j_current_left];
                            }
                        }

                        if (rightExp) {
                            // relax
                            betterApproximation = dist[i_parent_right][j_parent_right] + 1.0f - m[i_current_right][j_current_right];
                            if (dist[i_current_right][j_current_right] > betterApproximation) {
                                dist[i_current_right][j_current_right] = betterApproximation;
                                sim[i_current_right][j_current_right] = sim[i_parent_right][j_parent_right] + m[i_current_right][j_current_right];
                            }
                        }
                    }
                }

                if (i_offset == d_s) {
                    if (leftExp) {
                        currentSimilarity = sim[i_current_left][j_current_left];
                        if (currentSimilarity > accLeft)
                            accLeft = currentSimilarity;
                    }

                    if (rightExp) {
                        currentSimilarity = sim[i_current_right][j_current_right];
                        if (currentSimilarity > accRight)
                            accRight = currentSimilarity;
                    }
                }
            }
        }

        for(i_offset = 1; i_offset <= d_s; ++i_offset) {
            for (j_offset = 0; j_offset <= i_offset * (expRate - 1); ++j_offset) {
                i_current_right = i + i_offset;
                j_current_right = j + j_offset;
                i_current_left = i - i_offset;
                j_current_left = j - j_offset;

                leftExp = 0 <= i_current_left && i_current_left < Q && 0 <= j_current_left && j_current_left < R;
                rightExp = 0 <= i_current_right && i_current_right < Q && 0 <= j_current_right && j_current_right < R;

                if (leftExp) {
                    sim[i_current_left][j_current_left] = 0.0f;
                    dist[i_current_left][j_current_left] = std::numeric_limits<float>::max();
                }

                if (rightExp) {
                    sim[i_current_right][j_current_right] = 0.0f;
                    dist[i_current_right][j_current_right] = std::numeric_limits<float>::max();
                }
            }
        }
        sim[i][j] = 0.0f;
        dist[i][j] = std::numeric_limits<float>::max();

        return m[i][j] + accLeft + accRight;
    }

    am::AssociationMatrix noSeqSLAM(const am::AssociationMatrix& m, unsigned int d_s, int expRate) {

        d_s /= 2u;

        am::AssociationMatrix s{m.queryLength(), m.referenceLength()};

        am::AssociationMatrix dist{m.queryLength(), m.referenceLength(), std::numeric_limits<float>::max()};
        am::AssociationMatrix sim{m.queryLength(), m.referenceLength()};

        for(unsigned int q{d_s}; q < m.queryLength() - d_s; ++q)
            for(unsigned int r{0u}; r < m.referenceLength(); ++r)
                s[q][r] = shortestPathAssociation(m, dist, sim, q, r, d_s, expRate);

        return s;
    }

    void main() {

        const std::string path{"../results/noseqslam/associations/"};
        fl::createDirectory(path);

        am::AssociationMatrix matrices[] = {
                {"../results/overfeat/associations_boost/overfeat.out", true}
        };

        std::string names[] = {
                "Overfeat"
        };

        unsigned int d_sArr[] = {5, 7, 11, 15, 19, 31, 43, 51};
        int expRateArr[] = {2, 3};

        unsigned int index{0u};
        for(const am::AssociationMatrix& m: matrices) {
            for(unsigned int d_s: d_sArr) {
                for(int expRate: expRateArr) {
                    std::string name{names[index] + "_" + std::to_string(d_s) + "_" + std::to_string(expRate)};
                    std::cout << name << " is calculating" << std::endl;

                    Timer t{};
                    am::AssociationMatrix s = noSeqSLAM(m, d_s, expRate);
                    std::cout << "Elapsed time: " << t.secondsByFar() << std::endl;

                    s.saveToFile(path + name + "_noseqslam.out");
                    s.normalizeColumns();
                    img::saveImage(img::associationMatrixDrawingBW(s), path, name + "_noseqslam.png");
                }
            }
            ++index;
        }
    }

    void runningTimes() {
        const std::string path{"../results/noseqslam/running_times/"};
        fl::createDirectory(path);

        am::AssociationMatrix matrices[] = {
                {"../results/overfeat/associations_boost/overfeat.out", true}
        };

        int d_sArr[(51 - 5) / 2 + 1];
        int i{3};
        for (int& el: d_sArr)
            el = (i += 2);

        std::ofstream out{path + "times.out"};

        for(const am::AssociationMatrix& m: matrices) {
            for(unsigned int d_s: d_sArr) {
                Timer t{};
                am::AssociationMatrix s = noSeqSLAM(m, d_s, 2);
                out << d_s << "\t" << t.secondsByFar() << (d_s == 51 ? "" : "\n");
            }
        }

        out.close();
    }

}