//
// Created by jurica on 4/2/19.
//

#ifndef VISUALPLACERECOGNITION_SEQSLAM_H
#define VISUALPLACERECOGNITION_SEQSLAM_H

#include <array>
#include <iostream>

#include <AssociationMatrix.h>
#include <hog.h>
#include <img.h>
#include <types.h>

namespace seq {
    struct Sequence {
        // using SequencePair = std::pair<int, int>;

        unsigned int d_s;
        ty::matchPair* container;

        Sequence() = delete;

        Sequence(const Sequence& s);

        Sequence(Sequence&& s) noexcept;

        explicit Sequence(unsigned int d_s);
        Sequence(unsigned int d_s, const std::vector<int>& qIndices, const std::vector<int>& rIndices);

        Sequence& operator=(const Sequence& s);

        ~Sequence();

        ty::matchPair* begin();
        const ty::matchPair* begin() const;

        ty::matchPair* end();

        const ty::matchPair* end() const;

        ty::matchPair& operator[](int i);

        ty::matchPair& operator[](int i) const;

        Sequence& operator+=(const ty::matchPair& p);

        Sequence operator+(const ty::matchPair& p);

        int min_q() const;
        int max_q() const;

        float weight(const am::AssociationMatrix& m) const;

        bool contains(const ty::matchPair&) const;


    };

    std::ostream& operator<<(std::ostream& os, const Sequence& s);

    std::vector<Sequence>& operator+=(std::vector<Sequence>& v, std::pair<int, int> p);
    std::vector<Sequence> operator+(std::vector<Sequence>& v, std::pair<int, int> p);

    // C++ version of OpenSeqSLAM2.0 trajectoryOffsets function
    std::vector<Sequence> relativeIndices(unsigned int d_s, float v_min, float v_max, float v_step);

    // C++ version of OpenSeqSLAM2.0 matching function
    am::AssociationMatrix seqSLAM(const am::AssociationMatrix& m, unsigned int d_s, float v_min, float v_max, float v_step);
    am::AssociationMatrix seqSLAMCone(const am::AssociationMatrix& m, unsigned int d_s, float v_min, float v_max, float v_step);
    
    void main();
    void runningTimes();
}

#endif //VISUALPLACERECOGNITION_SEQSLAM_H
