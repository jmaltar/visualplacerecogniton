//
// Created by jurica on 22/03/19.
//

#ifndef VISUALPLACERECOGNITION_HOG_H
#define VISUALPLACERECOGNITION_HOG_H


#include <vector>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <algorithm>

#include <file.h>
#include <types.h>


namespace hog {
    // initialize HOGDescriptor OpenCV object
    cv::HOGDescriptor
    initializeHOG(int width, int height, int cSize = 32, int bSize = 64, int sSize = 32, int nrBins = 18);

    // compute HOG for a given image
    ty::imageRepresentation computeHOG(const cv::HOGDescriptor &hog, const cv::Mat &image);

    // original from http://www.juergenbrauer.org/old_wiki/doku.php?id=public:hog_descriptor_computation_and_visualization
    cv::Mat get_hogdescriptor_visu(const cv::Mat &color_origImg, ty::imageRepresentation &descriptorValues,
                                   const cv::Size &size, int cellSize, int gradientBinSize, float zoomFac);

    // generete image with hog arrows
    cv::Mat imageWithHOG(const cv::HOGDescriptor &hog, const cv::Mat &image, ty::imageRepresentation &descriptors);

    // usage example
    void HOGExample(const std::string imPath = "../data/freiburg_example/query/images/imageCompressedCam0_0005479.jpg",
                    int cSize = 32, int bSize = 64, int sSize = 32, int nrBins = 18);

    // generate HOG representation database
    std::vector<std::vector<float>>
    database(const std::string &path, int cSize = 32, int bSize = 64, int sSize = 32, int nrBins = 18);

    // generate HOG representation database and visualize it
    void visualizeEntireDatabase(const std::string &path, const std::string &newFolder, int cSize = 32, int bSize = 64,
                                 int sSize = 32, int nrBins = 18);

    void createAssociationMatrix(const std::string& referenceDBPath, const std::string& queryDBPath, const std::tuple<int, int, int, int, bool, int>& test, unsigned int& nr);

    void createAssociationMatrices(const std::string& referenceDBPath, const std::string& queryDBPath, unsigned int& nr);
}


#endif //VISUALPLACERECOGNITION_HOG_H
