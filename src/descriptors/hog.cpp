#include <hog.h>
#include <img.h>
#include <AssociationMatrix.h>

namespace hog {
    cv::HOGDescriptor initializeHOG(int width, int height, int cSize, int bSize, int sSize, int nrBins) {
        // https://stackoverflow.com/questions/44972099/opencv-hog-features-explanation

        cv::Size cellSize{cSize, cSize}; // cell W x H
        cv::Size blockSize{bSize, bSize}; // 2 x 2 W x H cells for each block
        cv::Size blockStride{sSize, sSize};

        // rescale width and height to be divisible by cellSize width and height
        cv::Size winSize{width / cellSize.width * cellSize.width, height / cellSize.height * cellSize.height};

        return cv::HOGDescriptor{winSize, blockSize, blockStride, cellSize, nrBins};
    }

    ty::imageRepresentation computeHOG(const cv::HOGDescriptor& hog, const cv::Mat& image) {
        ty::imageRepresentation descriptors;
        hog.compute(image, descriptors);
        return descriptors;
    }

    cv::Mat get_hogdescriptor_visu(const cv::Mat& color_origImg, ty::imageRepresentation& descriptorValues, const cv::Size & size, int cellSize, int gradientBinSize, float zoomFac ) {
        const int DIMX = size.width;
        const int DIMY = size.height;
        //float zoomFac = 10;
        cv::Mat visu;
        resize(color_origImg, visu, cv::Size( (int)(color_origImg.cols*zoomFac), (int)(color_origImg.rows*zoomFac) ) );

        //int cellSize        = 8;
        //int gradientBinSize = 9;
        float radRangeForOneBin = (float)(CV_PI/(float)gradientBinSize); // dividing 180 into 9 bins, how large (in rad) is one bin?

        // prepare data structure: 9 orientation / gradient strenghts for each cell
        int cells_in_x_dir = DIMX / cellSize;
        int cells_in_y_dir = DIMY / cellSize;
        float*** gradientStrengths = new float**[cells_in_y_dir];
        int** cellUpdateCounter   = new int*[cells_in_y_dir];
        for (int y=0; y<cells_in_y_dir; y++)
        {
            gradientStrengths[y] = new float*[cells_in_x_dir];
            cellUpdateCounter[y] = new int[cells_in_x_dir];
            for (int x=0; x<cells_in_x_dir; x++)
            {
                gradientStrengths[y][x] = new float[gradientBinSize];
                cellUpdateCounter[y][x] = 0;

                for (int bin=0; bin<gradientBinSize; bin++)
                    gradientStrengths[y][x][bin] = 0.0;
            }
        }

        // nr of blocks = nr of cells - 1
        // since there is a new block on each cell (overlapping blocks!) but the last one
        int blocks_in_x_dir = cells_in_x_dir - 1;
        int blocks_in_y_dir = cells_in_y_dir - 1;

        // compute gradient strengths per cell
        int descriptorDataIdx = 0;
        int cellx = 0;
        int celly = 0;

        for (int blockx=0; blockx<blocks_in_x_dir; blockx++)
        {
            for (int blocky=0; blocky<blocks_in_y_dir; blocky++)
            {
                // 4 cells per block ...
                for (int cellNr=0; cellNr<4; cellNr++)
                {
                    // compute corresponding cell nr
                    cellx = blockx;
                    celly = blocky;
                    if (cellNr==1) celly++;
                    if (cellNr==2) cellx++;
                    if (cellNr==3)
                    {
                        cellx++;
                        celly++;
                    }

                    for (int bin=0; bin<gradientBinSize; bin++)
                    {
                        float gradientStrength = descriptorValues[ descriptorDataIdx ];
                        descriptorDataIdx++;

                        gradientStrengths[celly][cellx][bin] += gradientStrength;

                    } // for (all bins)


                    // note: overlapping blocks lead to multiple updates of this sum!
                    // we therefore keep track how often a cell was updated,
                    // to compute average gradient strengths
                    cellUpdateCounter[celly][cellx]++;

                } // for (all cells)


            } // for (all block x pos)
        } // for (all block y pos)


        // compute average gradient strengths
        for (celly=0; celly<cells_in_y_dir; celly++)
        {
            for (cellx=0; cellx<cells_in_x_dir; cellx++)
            {

                float NrUpdatesForThisCell = (float)cellUpdateCounter[celly][cellx];

                // compute average gradient strenghts for each gradient bin direction
                for (int bin=0; bin<gradientBinSize; bin++)
                {
                    gradientStrengths[celly][cellx][bin] /= NrUpdatesForThisCell;
                }
            }
        }

        // draw cells
        for (celly=0; celly<cells_in_y_dir; celly++)
        {
            for (cellx=0; cellx<cells_in_x_dir; cellx++)
            {
                int drawX = cellx * cellSize;
                int drawY = celly * cellSize;

                int mx = drawX + cellSize/2;
                int my = drawY + cellSize/2;

                rectangle(visu, cv::Point((int)(drawX*zoomFac), (int)(drawY*zoomFac)), cv::Point((int)((drawX+cellSize)*zoomFac), (int)((drawY+cellSize)*zoomFac)), cv::Scalar(100,100,100), 1);

                // draw in each cell all 9 gradient strengths
                for (int bin=0; bin<gradientBinSize; bin++)
                {
                    float currentGradStrength = gradientStrengths[celly][cellx][bin];

                    // no line to draw?
                    if (currentGradStrength==0)
                        continue;

                    float currRad = bin * radRangeForOneBin + radRangeForOneBin/2;

                    float dirVecX = cos( currRad );
                    float dirVecY = sin( currRad );
                    float maxVecLen = (float)(cellSize/2.f);
                    float scale = 2.5; // just a visualization scale, to see the lines better

                    // compute line coordinates
                    float x1 = mx - dirVecX * currentGradStrength * maxVecLen * scale;
                    float y1 = my - dirVecY * currentGradStrength * maxVecLen * scale;
                    float x2 = mx + dirVecX * currentGradStrength * maxVecLen * scale;
                    float y2 = my + dirVecY * currentGradStrength * maxVecLen * scale;

                    // draw gradient visualization
                    line(visu, cv::Point((int)(x1*zoomFac),(int)(y1*zoomFac)), cv::Point((int)(x2*zoomFac),(int)(y2*zoomFac)), cv::Scalar(118,16,205), 1);

                } // for (all bins)

            } // for (cellx)
        } // for (celly)


        // don't forget to free memory allocated by helper data structures!
        for (int y=0; y<cells_in_y_dir; y++)
        {
            for (int x=0; x<cells_in_x_dir; x++)
            {
                delete[] gradientStrengths[y][x];
            }
            delete[] gradientStrengths[y];
            delete[] cellUpdateCounter[y];
        }
        delete[] gradientStrengths;
        delete[] cellUpdateCounter;

        return visu;

    }

    cv::Mat imageWithHOG(const cv::HOGDescriptor& hog, const cv::Mat& image, ty::imageRepresentation& descriptors) {
        float zoom = 1.0;
        return get_hogdescriptor_visu(image, descriptors, hog.winSize, hog.cellSize.width, hog.nbins, zoom);
    }

    void HOGExample(const std::string imPath, int cSize, int bSize, int sSize, int nrBins) {
        cv::Mat image = cv::imread(imPath, 1);
        cv::Mat imageGray;
        cvtColor(image, imageGray, cv::COLOR_BGR2GRAY);

        cv::HOGDescriptor hog = initializeHOG(image.size().width, image.size().height, cSize, bSize, sSize, nrBins);

        ty::imageRepresentation descriptors = computeHOG(hog, imageGray);

        cv::Mat withGradients = imageWithHOG(hog, image, descriptors);
        img::showImage(withGradients, "HOG");
    }

    std::vector<ty::imageRepresentation> database(const std::string& path, int cSize, int bSize, int sSize, int nrBins) {
        // output - HOG descriptors for each image
        std::vector<ty::imageRepresentation> db{};

        std::vector<std::string> paths = fl::filePaths(path);

        clock_t begin = clock();
        unsigned int index{0};
        for(const std::string& filePath: paths) {
            cv::Mat image = cv::imread(filePath, 1);
            cv::Mat imageGray;
            cvtColor(image, imageGray, cv::COLOR_BGR2GRAY);

            cv::HOGDescriptor hog = initializeHOG(image.size().width, image.size().height, cSize, bSize, sSize, nrBins);

            ty::imageRepresentation descriptors = computeHOG(hog, imageGray);
            db.push_back(descriptors);
        }
        // clock for running time insight only
        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Elapsed time: " << elapsed_secs << std::endl;

        return db;
    };

    void visualizeEntireDatabase(const std::string& path, const std::string& newFolder, int cSize, int bSize, int sSize, int nrBins) {
        // images
        std::vector<std::string> paths = fl::filePaths(path);

        // images hogs
        std::vector<ty::imageRepresentation> db = database(path, cSize, bSize, sSize, nrBins);

        fl::createDirectory(newFolder);

        unsigned int index{0};
        for(ty::imageRepresentation& descriptor: db) {
            cv::Mat image = cv::imread(paths[index++], 1);
            cv::Mat imageGray;
            cvtColor(image, imageGray, cv::COLOR_BGR2GRAY);
            cv::HOGDescriptor hog = initializeHOG(image.size().width, image.size().height, cSize, bSize, sSize, nrBins);
            cv::Mat imgWithHog = imageWithHOG(hog, image, descriptor);


            img::saveImage(imgWithHog, newFolder, std::to_string(index) + ".png");
        }
    }

    void createAssociationMatrix(const std::string& referenceDBPath, const std::string& queryDBPath, const std::tuple<int, int, int, int, bool, int>& test, unsigned int& nr) {
        std::cout << "Calculating reference DB descriptors" << std::endl;
        std::vector<ty::imageRepresentation> referenceDB = database(referenceDBPath, std::get<0>(test), std::get<1>(test), std::get<2>(test), std::get<3>(test));

        std::cout << "Calculating query DB descriptors" << std::endl;
        std::vector<ty::imageRepresentation> queryDB = database(queryDBPath,  std::get<0>(test), std::get<1>(test), std::get<2>(test), std::get<3>(test));

        std::cout << "Association matrix initialization" << std::endl;
        am::AssociationMatrix m{};

        clock_t begin = clock();

        std::cout << "Appending columns" << std::endl;
        m.appendAssociationColumns(referenceDB, queryDB, std::get<4>(test), std::get<5>(test));

        // clock for running time insight only
        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Elapsed time: " << elapsed_secs << std::endl;

        std::string params;
        params += std::to_string(std::get<0>(test)) + " ";
        params += std::to_string(std::get<1>(test)) + " ";
        params += std::to_string(std::get<2>(test)) + " ";
        params += "normalize ";
        std::get<4>(test) ? (params += "yes ") : (params += "no ");
        params += std::to_string(std::get<5>(test));

        m.saveToFile("../results/hog_test" + std::to_string(++nr) + "_" + params + ".out");

        cv::Mat img = img::associationMatrixDrawingBW(m, 5);
        const std::string name = "hog_test" + std::to_string(nr) + "_" + params + ".png";
        img::saveImage(img, "../results/", name);
    }

    void createAssociationMatrices(const std::string& referenceDBPath, const std::string& queryDBPath, unsigned int& nr) {
        std::tuple<int, int, int, int, bool, int> tests[13] = {
                { 16, 32, 16, 18, false, 0 },
                { 16, 32, 16, 9, false, 0 },
                { 16, 32, 16, 24, false, 0 },
                { 16, 64, 16, 18, false, 0 },
                { 32, 64, 32, 18, false, 0 },
                { 64, 64, 32, 18, false, 0 },
                { 16, 64, 16, 9, false, 0 },
                { 32, 64, 32, 9, false, 0 },
                { 16, 64, 16, 9, true, 3 },
                { 16, 64, 16, 9, true, 5 },
                { 16, 64, 16, 9, true, 11 },
                { 16, 64, 16, 9, true, 17 },
                { 16, 64, 16, 9, true, 25 },
        };

        for(const auto& test: tests)
            createAssociationMatrix(referenceDBPath, queryDBPath, test, nr);
    }
}

