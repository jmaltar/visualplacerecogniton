#include <iostream>
#include <fstream>

#include <overfeat.h>
#include <file.h>
#include <types.h>
#include <img.h>
#include <AssociationMatrix.h>
#include <torch/torch.h>

namespace overfeat {

    ty::imageRepresentation parseFile(const std::string& path) {
        std::ifstream ifs(path.c_str());
        int n, h, w;
        ifs >> n >> h >> w;
        ty::imageRepresentation imageDescriptor(n * h * w, 0.0f);

        unsigned int index{0u};
        while (!ifs.eof()) {
            float resp;
            ifs >> resp;
            imageDescriptor[index++] = resp;
        }
        return imageDescriptor;
    }

    std::vector<ty::imageRepresentation> parseDatabase(const std::string& path) {
        std::vector<std::string> paths = fl::filePaths(path);
        std::vector<ty::imageRepresentation> db(paths.size());

        unsigned int index{0};
        for(const std::string& filePath: paths) {
            db[index++] = parseFile(filePath);
        }

        return db;
    }

    std::vector<torch::Tensor> parseDatabaseCuda(const std::string& path) {
        std::vector<std::string> paths = fl::filePaths(path);
        std::vector<ty::imageRepresentation> db(paths.size());
        std::vector<torch::Tensor> tensors(paths.size());
        unsigned int index{0};
        for(const std::string& filePath: paths) {
            ty::imageRepresentation fileVector{parseFile(filePath)};
            long len{static_cast<long>(fileVector.size())};
            tensors[index++] = torch::from_blob(fileVector.data(), {1, len}).to(torch::device(torch::kCUDA));

        }
        return tensors;
    }

    am::AssociationMatrix createAssociationMatrix(const std::string& queryPath, const std::string& referencePath) {
        std::vector<ty::imageRepresentation> queryDB = parseDatabase(queryPath);
        std::vector<ty::imageRepresentation> referenceDB = parseDatabase(referencePath);
        am::AssociationMatrix m{};
        m.appendAssociationColumns(referenceDB, queryDB);
        return m;
    }



    am::AssociationMatrix createAssociationMatrixOnCuda(const std::string& queryPath, const std::string& referencePath) {

        unsigned int batchSize{32u};

        std::vector<torch::Tensor> referenceDBCuda = parseDatabaseCuda(referencePath);
        std::vector<std::string> queryDBPaths = fl::filePaths(queryPath);

        am::AssociationMatrix m{static_cast<unsigned int>(queryDBPaths.size()), static_cast<unsigned int>(referenceDBCuda.size()), 0.0f};

        unsigned int nrOfBatches{static_cast<unsigned int>(ceil(static_cast<double>(referenceDBCuda.size()) / static_cast<double>(batchSize)))};

        unsigned int queryIndex{0};
        for (const std::string& path: queryDBPaths) {

            ty::imageRepresentation queryVector = parseFile(path);
            long len{static_cast<long>(queryVector.size())};
            torch::Tensor query = torch::from_blob(queryVector.data(), {1, len}).to(torch::device(torch::kCUDA));

            torch::Tensor queryMultiple = torch::cat(std::vector<torch::Tensor>(batchSize, query));

            ty::associationMatrixColumn& column = m.container[queryIndex++];

            for (unsigned int batchNr{0}; batchNr < nrOfBatches; ++batchNr) {
                unsigned int lowerBound{batchNr * batchSize};

                unsigned int batchSizeInner;
                if (batchNr + 1 == nrOfBatches && (referenceDBCuda.size() % batchSize)) {
                    queryMultiple = torch::cat(std::vector<torch::Tensor>(referenceDBCuda.size() % batchSize, query));
                    batchSizeInner = referenceDBCuda.size() % batchSize;
                } else batchSizeInner = batchSize;

                unsigned int upperBound{lowerBound + batchSizeInner};

                torch::Tensor referenceMultiple = torch::cat(std::vector<torch::Tensor>(referenceDBCuda.begin() + lowerBound, referenceDBCuda.begin() + upperBound));


                torch::Tensor sim = torch::cosine_similarity(queryMultiple, referenceMultiple, 1);

                unsigned simIndex = 0;
                for(unsigned int referenceIndex{lowerBound}; referenceIndex < upperBound && referenceIndex < referenceDBCuda.size(); ++referenceIndex)
                    column[referenceIndex] = sim[simIndex++].item<float>();
            }
        }

        return m;
    }

}