#include <vector>
#include <string>

#include <types.h>
#include <AssociationMatrix.h>
#include <torch/torch.h>

namespace overfeat {

    std::vector<std::vector<float>> parseDatabase(const std::string& path);
    std::vector<torch::Tensor> parseDatabaseCuda(const std::string& path);

    am::AssociationMatrix createAssociationMatrix(const std::string& queryPath, const std::string& referencePath);
    am::AssociationMatrix createAssociationMatrixOnCuda(const std::string& queryPath, const std::string& referencePath);

}