//
// Created by jurica on 08/04/19.
//

#include <associationFunctions.h>


namespace af {
    float euclideanDistance(const ty::imageRepresentation& img1, const ty::imageRepresentation& img2) {

        float acc{0.0f};
        for(unsigned int i = 0; i < img1.size(); ++i)
            acc += powf(img1[i] - img2[i], 2.0f);

        return sqrtf(acc);
    }

    float cosineSimilarity(const ty::imageRepresentation& img1, const ty::imageRepresentation& img2) {
        // dot product
        float dotProduct{0.0f};
        // norm img 1
        float normImg1{0.0f};
        // norm img 2
        float normImg2{0.0f};

        for(unsigned int i = 0; i < img1.size(); ++i) {
            dotProduct += img1[i] * img2[i];
            normImg1 += powf(img1[i], 2.0f);
            normImg2 += powf(img2[i], 2.0f);
        }

        return dotProduct / (sqrtf(normImg1) * sqrtf(normImg2));
    }
}