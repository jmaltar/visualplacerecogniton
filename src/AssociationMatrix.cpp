//
// Created by jurica on 08/04/19.
//

#include <iostream>
#include <AssociationMatrix.h>

namespace am {

    AssociationMatrix::AssociationMatrix(): container{} {}

    AssociationMatrix::AssociationMatrix(const std::string &filePath, bool usingBoost): AssociationMatrix{} {
        if (usingBoost)
            fl::serializeFromFile(*this, filePath);
        else {
            std::ifstream ifs(filePath.c_str());

            int nrOfCols, nrOfRows;
            ifs >> nrOfCols >> nrOfRows;
            container = std::vector<ty::associationMatrixColumn>(nrOfCols, ty::associationMatrixColumn(nrOfRows, 0.0f));

            for(ty::associationMatrixColumn& column: container)
                for(float& association: column)
                    ifs >> association;
        }
    }

    AssociationMatrix::AssociationMatrix(unsigned int queryLength, unsigned int referenceLength, float initialValue): container(queryLength) {
        for(ty::associationMatrixColumn& column: container)
            column = ty::associationMatrixColumn(referenceLength, initialValue);
    }

    void AssociationMatrix::appendAssociationsColumn(const std::vector<ty::imageRepresentation>& referenceDB, const ty::imageRepresentation& queryImage, bool normalize, int normN) {
        ty::associationMatrixColumn column(referenceDB.size());

        unsigned int index{0};
        for(const ty::imageRepresentation& referenceImage: referenceDB)
            column[index++] = associationFunction(referenceImage, queryImage);

        container.push_back(column);
        if(normalize) {
            standardizeColumn(container.size() - 1, normN);
            normalizeColumn(container.size() - 1);
        }

    }

    void AssociationMatrix::appendAssociationColumns(const std::vector<ty::imageRepresentation>& referenceDB, const std::vector<ty::imageRepresentation> &queryDB, bool normalize, int normN) {
        for(const ty::imageRepresentation& queryImage: queryDB)
            appendAssociationsColumn(referenceDB, queryImage, normalize, normN);
    }

    unsigned int AssociationMatrix::queryLength() const { return container.size(); }

    unsigned int AssociationMatrix::referenceLength() const { return container[0].size(); }

    void AssociationMatrix::standardizeColumn(unsigned int i, unsigned int n) {
        // n represents neighborhood length
        // n should be odd && >= 3 (to do: assert) - the central element is the one we standardize
        ty::associationMatrixColumn& column = container[i];
        ty::associationMatrixColumn standardized(column.size(), 0.0f);
        int K = column.size();
        for(int k = 0; k < K; ++k) {
            float mean{0.0f};
            float stDev{0.0f};
            float nrNeigh{0.0f};

            int mnh = -static_cast<int>(n) / 2;
            int nh = static_cast<int>(n) / 2;

            for(int j = mnh; j <= nh; ++j) {
                int ind = k + j;
                0;
                if(k + j >= 0 && k + j < column.size()) {
                    ++nrNeigh;
                    mean += column[k + j];
                }
            }


            mean /= nrNeigh;

            for(int j = mnh; j <= nh; ++j)
                if(k + j >= 0 && k + j < column.size())
                    stDev += powf(column[k + j] - mean, 2.0f);

            stDev /= nrNeigh - 1.0f;
            stDev = sqrtf(stDev);

            standardized[k] = (column[k] - mean) / stDev;
        }

        column = standardized;
    }

    void AssociationMatrix::standardizeColumns(unsigned int n) {
        for(unsigned int i{0u}; i < queryLength(); ++i)
            standardizeColumn(i, n);
    }

    void AssociationMatrix::normalizeColumn(unsigned int i) {
        ty::associationMatrixColumn& column = container[i];
        float min = std::numeric_limits<float>::max();
        float max = std::numeric_limits<float>::min();
        for(float el: column) {
            if(el < min)
                min = el;
            if(el > max)
                max = el;
        }
        for(float& el: column)
            el = 1.0f / (max - min) * (el - max) + 1.0f;
    }

    void AssociationMatrix::normalizeColumns() {
        for(unsigned int i{0u}; i < queryLength(); ++i)
            normalizeColumn(i);
    }

    void AssociationMatrix::normalizeColumnL1(unsigned int i) {
        ty::associationMatrixColumn& column = container[i];
        float sum = std::accumulate(column.begin(), column.end(), 0.0f);
        if (sum != 0.0f)
            for (float& el: column) el /= sum;
    }

    void AssociationMatrix::normalizeColumnsL1() {
        for(unsigned int i{0u}; i < queryLength(); ++i)
            normalizeColumnL1(i);
    }

    float AssociationMatrix::maxAssociation() const {

        float max{0.0f};

        for(const ty::associationMatrixColumn& column: container)
            for(float el: column)
                if (el > max)
                    max = el;
        return max;
    }

    void AssociationMatrix::saveToFile(const std::string &filePath) const {
        fl::serializeToFile(*this, filePath);
    }

    ty::associationMatrixColumn& AssociationMatrix::operator[](int i) {
        return container[i];
    }

    const ty::associationMatrixColumn& AssociationMatrix::operator[](int i) const {
        return container[i];
    }

    std::pair<int, int> AssociationMatrix::match(unsigned int i) const {
        const ty::associationMatrixColumn column = container[i];
        int j = std::distance(column.begin(), std::max_element(column.begin(), column.end()));
        return std::make_pair(i, j);
    }

    std::vector<std::pair<int, int>> AssociationMatrix::matches() const {
        std::vector<std::pair<int, int>> m{};
        for(unsigned int i{0u}; i < queryLength(); ++i)
            m.push_back(match(i));
        return m;
    }

    bool AssociationMatrix::operator==(const am::AssociationMatrix& m) const {
        if (queryLength() != m.queryLength() || referenceLength() != m.referenceLength())
            return false;

        bool notEqualG = false;
        unsigned int notEqualCounter{0u};
        float accErr{0.0f};
        float diff;

        for(unsigned int i{0u}; i < queryLength(); ++i)
            for(unsigned int j{0u}; j < referenceLength(); ++j) {
                float first = container[i][j];
                float second = m[i][j];
                bool notEqual = first != second;
                if (notEqual) {
                    notEqualG = false;
                    ++notEqualCounter;
                    diff = abs(first - second);
                    accErr += diff;
                }
            }

        return notEqualG;
    }

}