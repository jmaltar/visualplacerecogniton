//
// Created by jurica on 23/03/19.
//

#ifndef VISUALPLACERECOGNITION_ASSOCIATIONMATRIX_H
#define VISUALPLACERECOGNITION_ASSOCIATIONMATRIX_H

#include <vector>
#include <functional>
#include <random>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <associationFunctions.h>

#include <file.h>
#include <types.h>

namespace am {
    struct AssociationMatrix {
        /*
            consists of differences/similarities between each query image
            with each image from reference database
            dimension: |D| x |Q| where D and Q are reference and query DBs
        */
        std::vector<ty::associationMatrixColumn> container;

        // difference/similarity measure between any two image representations
        ty::associationFunction associationFunction = af::cosineSimilarity;

        // make this class serializable
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int version) {
            ar & container;
        }

        // default constructor
        AssociationMatrix();
        // overloaded constructor (associations fetched from file)
        AssociationMatrix(const std::string& filePath, bool usingBoost=true);

        // overloaded constructor (allocate |D| x |Q| matrix)
        AssociationMatrix(unsigned int queryLength, unsigned int referenceLength, float initialValue=0.0f);

        // append  new column for some query image
        void appendAssociationsColumn(const std::vector<ty::imageRepresentation>& referenceDB, const ty::imageRepresentation& queryImage, bool normalize = false, int normN=0);

        // append  new column for each query image
        void appendAssociationColumns(const std::vector<ty::imageRepresentation>& referenceDB, const std::vector<ty::imageRepresentation>& queryDB, bool normalize = false, int normN=0);

        // query database cardinality
        unsigned int queryLength() const;
        // reference database cardinality
        unsigned int referenceLength() const;

        // standardize i-th column by procedure
        // proposed in the original SeqSLAM work
        void standardizeColumn(unsigned int i, unsigned int n);

        void standardizeColumns(unsigned int n);

        // translate to [0, 1] range
        void normalizeColumn(unsigned int i);

        // normalize all columns
        void normalizeColumns();

        // normalize (in L1 metric)
        void normalizeColumnL1(unsigned int i);

        // normalize all columns (in L1 metric)
        void normalizeColumnsL1();

        // as name suggests
        float maxAssociation() const;

        // save associations to file
        void saveToFile(const std::string& filePath) const;

        ty::associationMatrixColumn& operator[](int i);
        const ty::associationMatrixColumn& operator[](int i) const;

        // find the best match index given query index i
        std::pair<int, int> match(unsigned int i) const;

        // find all the best matches indices
        std::vector<std::pair<int, int>> matches() const;

        bool operator==(const AssociationMatrix&) const;

    };
}


#endif