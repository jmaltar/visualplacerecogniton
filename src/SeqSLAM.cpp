#include <SeqSLAM.h>
#include <fstream>
#include <timer.h>

namespace seq {

    Sequence::Sequence(const Sequence& s): d_s{s.d_s}, container{new ty::matchPair[s.d_s]} {
        for(unsigned int i{0u}; i < d_s; ++i)
            container[i] = s[i];
    }

    Sequence::Sequence(Sequence&& s) noexcept: d_s{s.d_s} {
        container = s.container;
        s.container = nullptr;
    }

    Sequence::Sequence(unsigned int d_s): container{new ty::matchPair[d_s]}, d_s{d_s} {}

    Sequence::Sequence(unsigned int d_s, const std::vector<int>& qIndices, const std::vector<int>& rIndices): Sequence{d_s} {
        unsigned int index{0u};
        for(ty::matchPair& i: *this) {
            i = std::make_pair(qIndices[index], rIndices[index]);
            ++index;
        }
    }

    Sequence& Sequence::operator=(const Sequence& s) {
        this->~Sequence();
        container = new ty::matchPair[s.d_s];
        d_s = s.d_s;
        for(unsigned int i{0u}; i < d_s; ++i)
            container[i] = s[i];
    }

    Sequence::~Sequence() { delete[] container; }

    ty::matchPair* Sequence::Sequence::begin() {
        return container;
    }

    const ty::matchPair* Sequence::begin() const {
        return container;
    }

    ty::matchPair* Sequence::end() {
        return container + d_s;
    }

    const ty::matchPair* Sequence::end() const {
        return begin() + d_s;
    }

    ty::matchPair& Sequence::operator[](int i) {
        return container[i];
    }

    ty::matchPair& Sequence::operator[](int i) const {
        return container[i];
    }

    Sequence& Sequence::operator+=(const ty::matchPair& p) {
        for(ty::matchPair& p_i: *this) {
            p_i.first += p.first;
            p_i.second += p.second;
        }
        return *this;
    }

    Sequence Sequence::operator+(const ty::matchPair& p) {
        Sequence s{*this};
        return s += p;
    }

    int Sequence::min_q() const {
        int min{std::numeric_limits<int>::max()};
        for(const ty::matchPair& p: *this)
            if(p.first < min)
                min = p.first;
        return min;
    }

    int Sequence::max_q() const {
        int max{std::numeric_limits<int>::min()};
        for(const ty::matchPair& p: *this)
            if(p.first > max)
                max = p.first;
        return max;
    }

    float Sequence::weight(const am::AssociationMatrix& m) const {
        return std::accumulate(this->begin(), this->end(), 0.0f, [&m](float acc, const ty::matchPair& p) {
            return acc + m[p.first][p.second];
        });
    }

    std::ostream& operator<<(std::ostream& os, const Sequence& s) {
        unsigned int index{0u};
        for(const ty::matchPair& i: s) {
            os << "(q_" << index << ", " << " r_" << index << ") = ("  << i.first << ", " << i.second << ")\t";
            ++index;
        }
        return os << std::endl;
    }

    std::vector<Sequence>& operator+=(std::vector<Sequence>& v, ty::matchPair p) {
        for(Sequence& s: v)
            s += p;
        return v;
    }

    std::vector<Sequence> operator+(std::vector<Sequence>& v, ty::matchPair p) {
        std::vector<Sequence> vNew{v};
        vNew += p;
        return vNew;
    }

    bool Sequence::contains(const ty::matchPair& sp) const {
        for (const ty::matchPair& p: *this)
            if (p == sp) return true;
        return false;
    }


    std::vector<Sequence> relativeIndices(unsigned int d_s, float v_min, float v_max, float v_step) {
        std::vector<int> q_indices{};
        for(int i = 0; i <= d_s - 1; ++i)
            q_indices.push_back(i - floorf(static_cast<float>(d_s) / 2.0f));

        std::vector<int> minTrajectory{};
        std::vector<int> maxTrajectory{};
        for(const int& i: q_indices) {
            minTrajectory.push_back(round(i * v_min));
            maxTrajectory.push_back(round(i * v_max));
        }

        int minMinT = *std::min_element(minTrajectory.begin(), minTrajectory.end());
        int minMaxT = *std::min_element(maxTrajectory.begin(), maxTrajectory.end());
        std::vector<float> uniqueVelocities{};
        for(int i = minMinT; i >= minMaxT; --i)
            uniqueVelocities.push_back(static_cast<float>(i) / floorf(static_cast<float>(d_s) / 2.0f) * -1.0f);

        std::vector<float> velocitiesTemp{};
        float currentVelocity = v_min;
        while (currentVelocity <= v_max) {
            velocitiesTemp.push_back(currentVelocity);
            currentVelocity += v_step;
        }

        std::vector<float> velocities{};
        for(float velocity: velocitiesTemp) {
            velocities.push_back([&uniqueVelocities](float velocity) -> float {
                float nearest = uniqueVelocities[0];
                for(float uniqueVelocity: uniqueVelocities)
                    if(abs(uniqueVelocity - velocity) < abs(nearest - velocity)) nearest = uniqueVelocity;
                return nearest;
            }(velocity));
        }

        std::sort(velocities.begin(), velocities.end());
        std::vector<float>::iterator it = std::unique(velocities.begin(), velocities.end());
        velocities.resize(std::distance(velocities.begin(), it));

        std::vector<Sequence> sequences{};

        for(float velocity: velocities) {
            std::vector<int> r_indices{};
            for(int i: q_indices)
                r_indices.push_back(round(i * velocity));

            sequences.push_back(Sequence{d_s, q_indices, r_indices});
        }

        return sequences;
    }

    // C++ version of OpenSeqSLAM2.0 matching function
    am::AssociationMatrix seqSLAM(const am::AssociationMatrix& m, unsigned int d_s, float v_min, float v_max, float v_step) {
        am::AssociationMatrix s{m.queryLength(), m.referenceLength()};

        std::vector<Sequence> seqRel = relativeIndices(d_s, v_min, v_max, v_step);

        int lowerBound = -seqRel[0].min_q();
        int upperBound = m.queryLength() - seqRel[0].max_q();

        for(int q{lowerBound}; q < upperBound; ++q) {
            for(int r{0}; r < m.referenceLength(); ++r) {
                // shift indices to their absolute index values
                std::vector<Sequence> seqAbs{seqRel + std::make_pair(q, r)};

                // remove those sequences with inappropriate reference indices
                seqAbs.erase(std::remove_if(seqAbs.begin(), seqAbs.end(), [&m](const Sequence& s) -> bool {
                    for(const ty::matchPair& p: s)
                        if(p.second < 0 || p.second >= m.referenceLength())
                            return true;
                    return false;
                }), seqAbs.end());

                if(seqAbs.empty()) break;

                std::vector<float> scores(seqAbs.size(), 0.0f);
                for(unsigned int i{0u}; i < seqAbs.size(); ++i)
                    scores[i] = seqAbs[i].weight(m);

                s[q][r] = *std::max_element(scores.begin(), scores.end());
            }
        }
        return s;
    }

    am::AssociationMatrix seqSLAMCone(const am::AssociationMatrix& m, unsigned int d_s, float v_min, float v_max, float v_step) {
        am::AssociationMatrix s{m.queryLength(), m.referenceLength(), 0.0f};

        std::vector<Sequence> seqRel = relativeIndices(d_s, v_min, v_max, v_step);

        int lowerBound = -seqRel[0].min_q();
        int upperBound = m.queryLength() - seqRel[0].max_q();

        // find best matches for unprocessed association matrix
        std::vector<ty::matchPair> matches = m.matches();

        for(int q{lowerBound}; q < upperBound; ++q) {
            for(int r{0}; r < m.referenceLength(); ++r) {
                // shift indices to their absolute index values
                std::vector<Sequence> seqAbs{seqRel + std::make_pair(q, r)};

                // remove those sequences with inappropriate reference indices
                seqAbs.erase(std::remove_if(seqAbs.begin(), seqAbs.end(), [&m](const Sequence& seq) -> bool {
                    for(const ty::matchPair& p: seq)
                        if(p.second < 0 || p.second >= m.referenceLength())
                            return true;
                    return false;
                }), seqAbs.end());

                if(seqAbs.empty()) break;

                // for each query index of the corresponding cone
                for(int k{seqAbs[0].min_q()}; k <=seqAbs[0].max_q(); ++k) {

                    // find its best match
                    const ty::matchPair& p = matches[k];
                    for (const Sequence& seq: seqAbs) // these two lines
                        if (seq.contains(p)) { // basically means - if cone includes best match in its k query index
                            // accumulate the association as proposed
                            s[q][r] += 1.0f / static_cast<float>(d_s);
                            // as many sequences can include this match, once when it is found, break this inner loop
                            // and continue to the next query index
                            break;
                        }
                }
            }
        }
        return s;
    }

    void main() {

        const std::string path{"../results/seqslam/associations/"};
        fl::createDirectory(path);

        am::AssociationMatrix matrices[] = {
                {"../results/overfeat/associations_boost/overfeat.out", true}
        };

        std::string names[] = {
                 "Overfeat"
        };

        unsigned int d_sArr[] = {5, 7, 11, 15, 19, 31, 43, 51};

        unsigned int index{0u};
        for(const am::AssociationMatrix& m: matrices) {
            for(unsigned int d_s: d_sArr) {
                std::string name{names[index] + "_" + std::to_string(d_s)};
                std::cout << name << " is calculating" << std::endl;

                Timer t{};

                am::AssociationMatrix s = seqSLAM(m, d_s, 0.0, 20.0, 0.25);

                std::cout << "Elapsed time: " << t.secondsByFar() << std::endl;

                s.saveToFile(path + name + "_seqslam.out");
                s.normalizeColumns();
                img::saveImage(img::associationMatrixDrawingBW(s), path, name + "_seqslam.png");
            }
            ++index;
        }
    }

    void runningTimes() {
        const std::string path{"../results/seqslam/running_times/"};
        fl::createDirectory(path);

        am::AssociationMatrix matrices[] = {
                {"../results/overfeat/associations_boost/overfeat.out", true}
        };

        int d_sArr[(51 - 5) / 2 + 1];
        int i{3};
        for (int& el: d_sArr)
            el = (i += 2);

        std::ofstream out{path + "times.out"};

        for(const am::AssociationMatrix& m: matrices) {
            for(unsigned int d_s: d_sArr) {
                Timer t{};
                am::AssociationMatrix s = seqSLAM(m, d_s, 0.0, 20.0, 0.25);
                out << d_s << "\t" << t.secondsByFar() << (d_s == 51 ? "" : "\n");
            }
        }

        out.close();
    }
}