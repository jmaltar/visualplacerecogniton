//
// Created by jurica on 4/2/19.
//

#ifndef VISUALPLACERECOGNITION_NOSEQSLAM_H
#define VISUALPLACERECOGNITION_NOSEQSLAM_H

#include <AssociationMatrix.h>

namespace noseq {
    float shortestPathAssociation(const am::AssociationMatrix& m, am::AssociationMatrix& dist, am::AssociationMatrix& sim, unsigned int i, unsigned int j, unsigned d_s, int expRate);
    am::AssociationMatrix noSeqSLAM(const am::AssociationMatrix& m, unsigned int d_s, int expRate);
    void main();
    void runningTimes();
}



#endif