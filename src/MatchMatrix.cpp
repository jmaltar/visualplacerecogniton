#include <MatchMatrix.h>
#include <iostream>

namespace mm {
    MatchMatrix::MatchMatrix(): container{} {}


    // all values to false positives, ground truth to true positives
    MatchMatrix::MatchMatrix(const std::string &filePath, unsigned int queryLength, unsigned int referenceLength): container(queryLength, ty::matchMatrixColumn(referenceLength, ty::match::FP)) {
        std::ifstream ifs(filePath.c_str());

        while (!ifs.eof()) {
            int queryIndex, nrOfGTs;

            ifs >> queryIndex >> nrOfGTs;

            int referenceIndex;
            for(int i = 0; i < nrOfGTs; ++i) {
                ifs >> referenceIndex;
                container[queryIndex][referenceIndex] = ty::match::TP;
            }
        }
    }

    MatchMatrix::MatchMatrix(const am::AssociationMatrix &m, const MatchMatrix &gt, float threshold): container(m.queryLength(), ty::matchMatrixColumn(m.referenceLength(), ty::match::TN)) {
        std::vector<std::pair<int, int>> matches = m.matches();

        for(const std::pair<int, int>& matchIndex: matches) {
            float association = m[matchIndex.first][matchIndex.second];

            if (association >= threshold) {
                for(int seg = -3; seg <= 3; ++seg) {
                    if (matchIndex.second + seg >= 0 && matchIndex.second + seg < referenceLength())
                        if(gt[matchIndex.first][matchIndex.second + seg] == ty::match::TP) {
                            container[matchIndex.first][matchIndex.second] = ty::match::TP;
                            break;
                        }
                }

                if (container[matchIndex.first][matchIndex.second] != ty::match::TP)
                    container[matchIndex.first][matchIndex.second] = ty::match::FP;
            }
            else container[matchIndex.first][matchIndex.second] = ty::match::FN;
        }
    }

    ty::matchMatrixColumn& MatchMatrix::operator[](int i) { return container[i]; }
    const ty::matchMatrixColumn& MatchMatrix::operator[](int i) const { return container[i]; }

    unsigned int MatchMatrix::queryLength() const { return container.size(); }

    unsigned int MatchMatrix::referenceLength() const { return container[0].size(); }

    std::pair<float, float> precisionAndRecall(const mm::MatchMatrix &n) {
        float falseNegatives{0.0f};
        float falsePositives{0.0f};
        float truePositives{0.0f};

        for(const ty::matchMatrixColumn& column: n.container)
            for(const ty::match& el: column)
                if (el == ty::match::FN) {
                    ++falseNegatives;
                    break;
                } else if (el == ty::match::FP) {
                    ++falsePositives;
                    break;
                } else if (el == ty::match::TP) {
                    ++truePositives;
                    break;
                }

        // initially, there is 100% precision at 0% recall
        float precision{1.0f};
        float recall{0.0f};

        if ((truePositives + falsePositives) > 0.0f)
            precision = truePositives / (truePositives + falsePositives);

        if((truePositives + falseNegatives) > 0.0f)
            recall = truePositives / (truePositives + falseNegatives);

        return std::make_pair(precision, recall);
    }

    std::vector<std::pair<float, float>> precisionAndRecallVector(const am::AssociationMatrix& m, const MatchMatrix& gt) {
        std::vector<std::pair<float, float>> precisionsAndRecalls{};

        auto max = m.maxAssociation();
        float steps = 100.0f;

        float acc = 0.0f;
        for(int i = 0; i < static_cast<int>(steps); ++i) {
            acc += max / steps;
            mm::MatchMatrix res{m, gt, acc};
            std::pair<float, float> prRes = mm::precisionAndRecall(res);
            precisionsAndRecalls.push_back(prRes);
        }

        return precisionsAndRecalls;

    }

    void precisionAndRecallExperiment(const std::string& root, const std::string& destination) {
        fl::createDirectory(destination);
        // list folders containing association files
        std::vector<std::string> associations = fl::listDirectory(root);
        // for each association
        for(const std::string& association: associations) {
            // check if association is path to .out file
            if (fl::extension(association) != "out") continue;
            // parse name
            const std::string name{fl::split(fl::split(association, '/').back(), '.').front()};
            // build match matrix
            am::AssociationMatrix m{association, true};
            // GT
            mm::MatchMatrix gt{"../data/bonn_example/gt_bonn_example.out", m.queryLength(), m.referenceLength()};
            // get precisions and recalls
            std::vector<std::pair<float, float>> precisionsAndRecalls = mm::precisionAndRecallVector(m, gt);
            // save
            fl::savePrecisionAndRecall(destination, name + ".out", precisionsAndRecalls);
            std::cout << "Saved precision and recall: " << name << std::endl;
        }
    }

}

