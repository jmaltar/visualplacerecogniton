//
// Created by jurica on 23/03/19.
//

#ifndef VISUALPLACERECOGNITION_ASSOCIATIONFUNCTIONS_H
#define VISUALPLACERECOGNITION_ASSOCIATIONFUNCTIONS_H

#include <cmath>
#include <vector>
#include <functional>

#include <types.h>

namespace af {

    float euclideanDistance(const ty::imageRepresentation& img1, const ty::imageRepresentation& img2);
    float cosineSimilarity(const ty::imageRepresentation& img1, const ty::imageRepresentation& img2);
}

#endif //VISUALPLACERECOGNITION_ASSOCIATIONFUNCTIONS_H
