cmake_minimum_required(VERSION 3.10)
project(VisualPlaceRecognition)

set(CMAKE_CXX_STANDARD 17)

include_directories(./src ./src/descriptors ./src/utility)

add_executable( VisualPlaceRecognition
        src/main.cpp
        src/utility/img.cpp
        src/utility/timer.cpp
        src/AssociationMatrix.cpp
        src/MatchMatrix.cpp
        src/SeqSLAM.cpp
        src/NOSeqSLAM.cpp
        src/associationFunctions.cpp
        src/descriptors/hog.cpp
        src/utility/file.cpp
        src/utility/types.h
        src/descriptors/overfeat.cpp)

# OpenCV
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )

#Boost
find_package(Boost 1.65.1 COMPONENTS system filesystem serialization REQUIRED)
include_directories( ${Boost_INCLUDE_DIR} )

#Torch
find_package(Torch REQUIRED)

target_link_libraries( VisualPlaceRecognition LINK_PUBLIC ${Boost_LIBRARIES} ${OpenCV_LIBS} ${TORCH_LIBRARIES})