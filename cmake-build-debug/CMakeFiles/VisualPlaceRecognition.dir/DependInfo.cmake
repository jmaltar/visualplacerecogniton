# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/jurica/Desktop/noseqslam/src/AssociationMatrix.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/AssociationMatrix.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/MatchMatrix.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/MatchMatrix.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/NOSeqSLAM.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/NOSeqSLAM.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/SeqSLAM.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/SeqSLAM.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/associationFunctions.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/associationFunctions.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/descriptors/hog.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/descriptors/hog.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/descriptors/overfeat.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/descriptors/overfeat.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/main.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/main.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/utility/file.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/utility/file.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/utility/img.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/utility/img.cpp.o"
  "/home/jurica/Desktop/noseqslam/src/utility/timer.cpp" "/home/jurica/Desktop/noseqslam/cmake-build-debug/CMakeFiles/VisualPlaceRecognition.dir/src/utility/timer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "AT_PARALLEL_OPENMP=1"
  "_THP_CORE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  ".././src"
  ".././src/descriptors"
  ".././src/utility"
  "/usr/local/include/opencv4"
  "/home/jurica/libtorch/include"
  "/home/jurica/libtorch/include/torch/csrc/api/include"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
